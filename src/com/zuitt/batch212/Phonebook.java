package com.zuitt.batch212;

import java.util.ArrayList;

public class Phonebook {

    // properties
    private ArrayList<Contact> contacts = new ArrayList<>();

    // empty constructor
    public Phonebook(){}

    // parameterized constructor
    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }

    // getters
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    // setters
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    // method
    public void getPhonebook() {

        String details = "";
        String space = "";

        if(this.contacts.size() == 0) {
            details = "No Contacts found.";
        }
        else {
            for ( Contact contact: contacts) {
                if(details.length() == 0){
                    space = "";
                }
                else {
                    space = "\n";
                }
                details += space;

                // name
                details += "\n" + contact.getName() + "\n" + "--------";

                // numbers
                details += "\n" + contact.getName() + " has the following registered numbers:";
                for (String number: contact.getNumbers()) {
                    details += "\n" + number;
                }
                details += "\n" + "--------";

                // address
                details += "\n" + contact.getName() + " has the following registered addresses:";
                for (String address: contact.getAddresses()) {
                    details += "\n" + address;
                }
                details += "\n" + "========";
            }
        }
        System.out.println(details);
    }
}
