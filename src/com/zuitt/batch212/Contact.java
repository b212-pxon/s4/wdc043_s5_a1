package com.zuitt.batch212;

import java.util.ArrayList;

public class Contact {

    // properties
    private String name;
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    // empty constructor
    public Contact(){}

    // parameterized constructor
    public Contact(String name, ArrayList numbers, ArrayList addresses){
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    // getters
    public String getName(){
        return this.name;
    }
    public ArrayList<String> getNumbers() {
        return this.numbers;
    }

    public ArrayList<String> getAddresses() {
        return this.addresses;
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }
    public void setNumbers(String number) {
        this.numbers.add(number);
    }
    public void setAddresses(String address) {
        this.addresses.add(address);
    }
}
