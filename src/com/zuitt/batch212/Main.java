package com.zuitt.batch212;

public class Main {

    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        // John Doe
        Contact johnDoe = new Contact();
        johnDoe.setName("John Doe");
        johnDoe.setNumbers("+639152468596");
        johnDoe.setNumbers("+639228547963");
        johnDoe.setAddresses("my home in Quezon City");
        johnDoe.setAddresses("my office in Makati City");

        phonebook.setContacts(johnDoe);

        // Jane Doe
        Contact janeDoe = new Contact();
        janeDoe.setName("Jane Doe");
        janeDoe.setNumbers("+639162148573");
        janeDoe.setNumbers("+639173698541");
        janeDoe.setAddresses("my home in Caloocan City");
        janeDoe.setAddresses("my office in Pasay City");

        phonebook.setContacts(janeDoe);

        // phonebook
        phonebook.getPhonebook();

    }

}
